'''

	This is a file that is designed to extract multipple channels from pepper that i can use to experement with bandforming.

'''


import soundfile as sf
import qi
import numpy as np
import time
import argparse
import struct

class all_mics(object):

	def __init__(self, app):

		super(all_mics, self).__init__()

		self.session = app.session

		self.recording = np.array([])

		self.module_name = "all_mics"

		#ask for the front microphone signal sampled at 16kHz
		self.session.registerService(self.module_name, self)
		self.audio_service = session.service("ALAudioDevice")
		self.audio_service.setClientPreferences(self.module_name, 16000, 0, 0) #alst arg is de-interleaving requested
		self.audio_service.subscribe(self.module_name)

		self.is_running = False

		self.AL = self.session.service("ALAutonomousLife")


	def processRemote(self, nbOfChannels, nbOfSamplesByChannel, timeStamp, inputBuffer):

		#will record the whole thing to a wav file
		if self.is_running:

			#this should turn the whole this into a long vecter
			try:
				
				vector = np.asarray(struct.unpack('<{}h'.format(nbOfSamplesByChannel * nbOfChannels), inputBuffer)).astype(np.float32) / (3276.7 * 2)
			
			except Exception as e:

				print e

			#print(vector)

			#vector = inputBuffer

			self.recording = np.append(self.recording, vector)


	def start(self):
		self.AL.setAutonomousAbilityEnabled("All", False)
		self.is_running = True
		return


	def stop(self):
		self.AL.setAutonomousAbilityEnabled("All", True)
		self.is_running = False
		save_as_wav(self.recording)
		return


def save_as_wav(data):

	file_name = 'two_speakers_far.wav'

	sf.write(file_name, data, 16000)


if __name__ == '__main__':

		#set up pepper session
	parser = argparse.ArgumentParser()
	parser.add_argument("--ip", type=str, default="198.18.0.1",
	                    help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
	parser.add_argument("--port", type=int, default=9559,
	                    help="Naoqi port number")

	args = parser.parse_args()

	try:
	    # Initialize qi framework.
	    connection_url = "tcp://" + args.ip + ":" + str(args.port)
	    app = qi.Application(["all_mics", "--qi-url=" + connection_url])
	except RuntimeError:
	    print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
	           "Please check your script arguments. Run with -h option for help.")
	    sys.exit(1)

	app.session.connect('192.168.0.116')
	session = app.session

	recorder = all_mics(app)

	recorder.start()

	time.sleep(10)

	recorder.stop()




